mainApp.controller('mainController',function($scope,$http,$window){

  $('#viewport').css({'min-height':$($window).height()-244});
    
    $( window ).resize(function() {
      $('#viewport').css({'min-height':$($window).height()-244});   
    });

    $http.get('clients-filter.json')
       .then(function(data){
          console.log(data.data.clients);   
            $scope.clientData = data.data.clients;
            $scope.items =  $scope.clientData;
        });
  
  $scope.increaseLimit = function () {
    if ($scope.limit < $scope.items.length) {
      $scope.limit += 50;
    }
  };
});