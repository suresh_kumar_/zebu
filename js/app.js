var mainApp = angular.module('mainApp', ['ngRoute']);

mainApp.config(function($routeProvider, $locationProvider){
//    $locationProvider.html5Mode(true).hashPrefix('');
    $locationProvider.hashPrefix('');
    $routeProvider.when('/', {
        templateUrl: './pages/main.html',
        controller: 'mainController'
    });
    $routeProvider.otherwise({
        redirectTo: '/'
    });
});